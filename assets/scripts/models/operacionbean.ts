
export const OPERADORES=['+','-','*'];
export const OPERADOR={
    '+':function(x,y){return x+y},
    '-': function(x,y){return x-y},
    '*': function(x,y){return x*y}
}
export class Operacion{
    public static SUMAS_Y_RESTAS: string ='SUMASYRESTAS';
    public static SUMAS_RESTAS_MULTIPLICACIONES: string ='SUMASRESTASYMULTIPLICACIONES';

    operandos: number[];
    operadores: string[];

    private limite: number = 9;
    private minimo: number = 2;
    private limiteoperadores: number = 2;
    

    constructor(minimo: number, limite: number,tipo: string, repeticion: number){
        if(tipo === Operacion.SUMAS_Y_RESTAS){
            this.limiteoperadores = 2;
        }else if(tipo === Operacion.SUMAS_RESTAS_MULTIPLICACIONES){
            this.limiteoperadores = 3;
        
        }
        this.limite=limite-1;
        if(typeof(minimo) != 'undefined' ){
            cc.log(minimo);
        this.minimo = minimo;
        }
        
        this.operandos=[];
        this.operadores=[];
        //Generamos el número inicial
        this.operandos.push(this.generarNumero());  
        //Generamos el operador
        this.operadores.push(this.generarOperando());

        //Vamos a generar el segundo operando
        let n2=0;
        let operastr: string='vacia';
        do{

            n2=this.generarNumero();
            //Vamos a comprobar resta
            if(this.operadores[0] === '-' && n2> this.operandos[0]){
                let t=n2;
                n2=this.operandos[0];
                this.operandos[0]=t;
            }
             
        }while(OPERADOR[this.operadores[0]](this.operandos[0],n2)<=0);

        this.operandos.push(n2);
    }

    private generarNumero(): number{
        let n:number = 0;
       
       
            let n=Math.floor(cc.random0To1()*(this.limite-this.minimo+1))+(this.minimo);
            
            cc.log('Generamos el numero '+n.toString());
        
        
        
        
        return n;
    }

    private generarOperando(): string{
        let n=Math.floor(cc.random0To1()*this.limiteoperadores);
        cc.log('Generamos el operando '+OPERADORES[n]);
        return OPERADORES[n];

    }

    public getElementos(): string[]{
        let elementos=[];
        for (const key in this.operadores) {
            elementos.push(this.operadores[key]);
        }
        for (const k in this.operandos) {
            elementos.push(this.operandos[k].toString());
        }
        return elementos;
    }
    public contarElemento(elemento: string): number{
        cc.log(this.operandos);
        let elementos=this.operandos.filter( (n: number) => {
            return (n.toString() == elemento);
        })
        return elementos.length;
    }

    public getResultado(): string{
        let r: number=OPERADOR[this.operadores[0]](this.operandos[0],this.operandos[1]);
        return r.toString();
     }

}