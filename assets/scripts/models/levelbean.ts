import { Operacion } from './operacionbean';
export class NivelJuego{
   id: string;
    minimo?: number = 2;
    tope: number;
    tipo: string;
    nombre: string;
    
    numerosrepetidos?: number = 2;
    
}

export const NIVELES: NivelJuego[]=[
{
    id: '1',
    tope: 10,
    tipo: Operacion.SUMAS_Y_RESTAS,
    nombre: 'Sumas y restas de 1 a 10',
},
{
    id: '2',
    tope: 20,
    minimo: 5,
    tipo: Operacion.SUMAS_Y_RESTAS,
    nombre: 'Sumas y restas de 5 a 20',
},
{
    id: '3',
    tope: 50,
    minimo: 10,
    tipo: Operacion.SUMAS_Y_RESTAS,
    nombre: 'Sumas y restas de 10 a 50',
},

//SEGUNDO NIVEL
{
    id: '4',
    tope: 11,
    tipo: Operacion.SUMAS_RESTAS_MULTIPLICACIONES,
    nombre: 'Sumas, restas y multiplicaciones de 1 a 11',
},

{
    id: '5',
    tope: 20,
    tipo: Operacion.SUMAS_RESTAS_MULTIPLICACIONES,
    nombre: 'Sumas, restas y multiplicaciones de 1 a 20',
},

{
    id: '6',
    tope: 25,
    minimo: 10,
    tipo: Operacion.SUMAS_RESTAS_MULTIPLICACIONES,
    nombre: 'Sumas, restas y multiplicaciones de 10 a 25',
}

];