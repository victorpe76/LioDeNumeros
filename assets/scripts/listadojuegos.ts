import { Event } from './../../creator.d';
import { NIVELES } from "./models/levelbean";
import ItemJuego from "./itemjuego";
import LevelManager from './LevelManager';
import JuegoLio from './juego';
import { Puntuacion } from './models/puntuacionbeans';
import SceneBackComponent from './SceneBackComponent';

// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends SceneBackComponent {
    
    
    @property(cc.Prefab)
    itemjuego: cc.Prefab;

    generarListado(): any {
        //Obtenemos los jugados
        let puntuaciones: Puntuacion[]=JSON.parse(cc.sys.localStorage.getItem('puntuaciones'));
        let contenedoritems: cc.Node = this.node.getChildByName('contenedoritems');
       cc.log(puntuaciones);
        for (const j in NIVELES) {
            let ij=cc.instantiate(this.itemjuego);
            ij.getComponent(ItemJuego).setNivel(NIVELES[j]);
            ij.on(cc.Node.EventType.TOUCH_END,this.onItemClick)
            ij.setPositionX(0);
            
            
            if(puntuaciones != undefined && puntuaciones.find((p: Puntuacion) => p.nivel == NIVELES[j].id) != undefined){
               cc.log("JUEGO REALIZADO");
                ij.getComponent(ItemJuego).isOK(true);
            }else{
                cc.log("JUEGO NO REALIZADO");
                ij.getComponent(ItemJuego).isOK(false);
            }
            contenedoritems.addChild(ij);
            contenedoritems.getComponent(cc.Layout).updateLayout();
        }
    }

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        super.onLoad();
        this.generarListado();
    },

    onItemClick(evento: Event){
        cc.log(evento.currentTarget);
       let nivel = (evento.currentTarget as cc.Node).getComponent(ItemJuego).nivel;
       cc.director.loadScene('tablero',(error,data) => {
            let tablero: cc.Scene= cc.director.getScene();
            tablero.getChildByName('principal').getComponent(LevelManager).playlevel=nivel;
            tablero.getChildByName('principal').getComponent(JuegoLio).nextGame();
        })
    }
    start () {

    },

    // update (dt) {},
    backToPrincipal(){
        cc.director.loadScene('principal');
    }
    onTapBack(){
        this.backToPrincipal();
    }
}
