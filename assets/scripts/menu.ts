// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.addBackSupport();
    }

    start () {

    },

    // update (dt) {},

    public jugar(){
        
        cc.director.loadScene('juegos');
    }
    public mostrarTiempos(){
        cc.director.loadScene('puntuaciones');
    }
    public mostrarInstrucciones(){
        
        cc.director.loadScene('instrucciones');
    }

    private closeGame(){
        cc.game.end();
    }
    addBackSupport(): any {

        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function(keyCode, event) {
                if ((keyCode == cc.KEY.back) || (keyCode == cc.KEY.backspace) || (keyCode == cc.KEY.escape))
                    this.closeGame();
            }.bind(this)
        }, this.node);
    }
}
