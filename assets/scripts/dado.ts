import { EventTouch, EventTarget } from './../../creator.d';

import Numeros from "./numeros";

// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Dado extends cc.Component {

    

    @property
    valor: string = 'hello';
    @property
    public isdraggable: boolean = false;

    private originalPosition: cc.Vec2;
    private originalGroup: string;
    private numerosnode: cc.Node;
    private huecoasginado: cc.Node = null;
    dadomoving: boolean = false;
    dadoenhueco: boolean = false;
    selectedtarget: cc.Node = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.numerosnode = this.node.parent;
        this.enableDrag();
        
        
    }

    public enableDrag(){
        if(this.isdraggable){
            this.node.on(cc.Node.EventType.TOUCH_START,(event: EventTouch) => {
                cc.log("Iniciado el touch")
cc.log(event)
                let target = this.node;
                let locationInNode = target.convertToNodeSpace(event.touch.getLocation());
            let s = target.getContentSize();
            let  rect = cc.rect(0, 0, s.width, s.height);
            if (cc.rectContainsPoint(rect, locationInNode)) {
                cc.log("sprite began... x = " + locationInNode.x + ", y = " + locationInNode.y);
                
                this.dadomoving = true;
                this.node.opacity = 120;
                this.node.scale=1.2;
                
            }
                
                
            });
            
            this.node.on(cc.Node.EventType.TOUCH_MOVE,(event: EventTouch) => {
                if(this.dadomoving ){
                
                    let delta:cc.Vec2=event.touch.getDelta();
                    let currposition:cc.Vec2 = this.node.getPosition();

                    this.node.setPosition(currposition.x+delta.x,
                    currposition.y+delta.y);
                }
            });
            this.node.on(cc.Node.EventType.TOUCH_END,(event: EventTouch) => {
                cc.log("Finaliza el touch");
                this.node.opacity=255;
                this.node.scale=1;
                this.dadomoving=false;
                //De momento lo retorno a su posicion original
                if(this.huecoasginado === null || this.node.group === 'dadocolocado'){

                    if(this.node.group === 'dadocolocado'){
                        cc.log('Vamos a resetear hueco');
                        
                        this.retornarNumeros();

                        cc.log("hueco");
                        cc.log(this.huecoasginado);
                    }
                        this.node.setPosition(this.originalPosition.x,this.originalPosition.y);
                
                    return;
                   
                }else{
                    this.colocardado();
                }
            })

        
    }
    }
    public setValor(valor: string){
        this.node.getChildByName("numero").getComponent(cc.Label).string=valor;
        
        /*let handler: cc.Component.EventHandler=new cc.Component.EventHandler();
        handler.target=this.node;
        handler.component="dado";
        handler.handler="clicDado";
        */
        this.valor=valor;
    //this.node.getComponent(cc.Button).clickEvents.push(handler)
    }

    start () {

    },

    public clicDado(valor){
        cc.log("Has picado en un dado "+this.valor);
       
        this.node.parent.getComponent(Numeros).seleccionarDado(this.node);
        
    }
    
    public initGroup(grupo: string){
        this.originalGroup=grupo;
        this.setGroup(grupo);
    }

    public setGroup(grupo: string){
        this.node.group=grupo;
    }

    public storePosition(){
        this.originalPosition = new cc.Vec2();
        this.originalPosition.x=this.node.getPosition().x;
        this.originalPosition.y=this.node.getPosition().y;
    }

    onCollisionEnter(other, self) {
        if(this.huecoasginado != other.node){
            cc.log("Hemos colisionado con un hueco nuevo");
            //Relajamos el antiguo hueco
            if(this.huecoasginado!=null){
                this.huecoasginado.scale=1;
            }
            let hueconode: cc.Node = other.node;
            hueconode.scale=1.2;

            this.huecoasginado=hueconode;
        }
        
    }

    onCollisionStay(other, self) {
       //cc.log("seguimos colisionado");

       if(!this.dadomoving){
           
            
           // this.enableDrag();
           // cc.log(this.node);
           // cc.log(resultados)
           //cc.log("Me han soltado y estoy colisionando");
       }
    }
    public retornarNumeros(){
        this.node.removeFromParent(false);
        this.numerosnode.addChild(this.node);
        this.reset();
    }
    onCollisionExit(other, self) {
        cc.log("Dejamos de colisionar");
        this.dadoenhueco=false;
        let hueconode: cc.Node = other.node;
        hueconode.scale=1;
        if(this.node.group!='dadocolocado'){
            cc.log('Asignamos hueco nulo');
            this.huecoasginado = null;
        }
        
    }



    public reset(){
        this.setGroup(this.originalGroup);
        this.node.setPosition(this.originalPosition);
        this.resetHueco();
    }

    public resetHueco(){
        if(this.originalGroup === 'dado'){
            this.huecoasginado.group='hueco';
        }else if(this.originalGroup === 'dador'){
            this.huecoasginado.group='huecor';
        }
        cc.log('Asignamos hueco nulo');
        this.huecoasginado = null;
        
    }

    private colocardado(){
        cc.log("Colocamos el dado");
        this.node.group='dadocolocado';
           
        
        let resultados: cc.Node = this.huecoasginado.parent;
        
        this.node.removeFromParent(false)
        resultados.addChild(this.node);
        
        this.node.setPosition(this.huecoasginado.getPosition());
        this.huecoasginado.group='huecoocupado';
       // this.node.opacity = 255;
       //this.node.setLocalZOrder(100);
       // this.node.getComponent(Dado).isdraggable = true;
       cc.log("hueco");
                        cc.log(this.huecoasginado);
         cc.log("Reproducimos el sonido");
        this.getComponent(cc.AudioSource).play();
    }
    
    // update (dt) {},
}
