import { NivelJuego, NIVELES } from './models/levelbean';
// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class LevelManager extends cc.Component {

    private static NOPERACIONES : number[] = [3,4,5,0];
    @property
    currentlevel: number = 0

    playlevel: NivelJuego = null;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    }

    start () {

    },

    public siguienteLevel(): number{
        this.currentlevel++;
        return LevelManager.NOPERACIONES[this.currentlevel-1];
    }
    // update (dt) {},
}
