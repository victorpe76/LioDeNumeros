import { OPERADORES, OPERADOR } from './models/operacionbean';
import Dado from "./dado";
import JuegoLio from './juego';


// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class OperacionComponente extends cc.Component {

  

    @property(cc.Prefab)
    molde: cc.Prefab = null;

    @property
    text: string = 'hello';

    cuentadados: number = 0;

    huecos: cc.Node[]= [];
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.log("Creamos los huecos");
        for(let n=0;n<5;n++){
            this.crearHueco(n);
        }
        //Cambiamos el grupo del resultado
        this.huecos[4].group = 'huecor';
        //Ponemos el dado del igual

        this.node.getChildByName('dado').setPosition(this.huecos[3].getPosition());
        this.node.getChildByName('dado').setLocalZOrder(10);
        //Quitamos el hueco del igual
        this.huecos[3].destroy();
        //Establecemos el listener para cuando se añadan dados
        this.node.on('child-added',(event) => {
            cc.log("Añadido elemento");
            setTimeout(() => 
{
    this.comprobarOperacion();
},
300);
           
        });
    }
    comprobarOperacion(){
       let dadosoperacion = this.node.children.filter((hijo: cc.Node) => hijo.group === 'dadocolocado' )
       //cc.log(dadosoperacion);
       if(dadosoperacion.length === 4) {
           dadosoperacion = dadosoperacion.sort((a:cc.Node, b:cc.Node) => {
               if(a.getPositionX()>b.getPositionX()){
                   return 1;
               }else if(a.getPositionX()<b.getPositionX()){
                    return -1;
               }else{
                    return 0;
               }
              
           });
           //dadosoperacion.push(this.node.children.find((d: cc.Node)=> d.group === 'dadorcolocado'));
           let operadores=[];
           for (const k in dadosoperacion) {
               cc.log(dadosoperacion[k].getPositionX())
               cc.log(dadosoperacion[k].color);
               operadores.push(dadosoperacion[k].getComponent(Dado).valor);
               cc.log(dadosoperacion[k].getComponent(Dado).valor)

           }

           
           if(this.calcular(operadores) === + operadores[3]){
               cc.log("Operacion correcta");
               this.operacionCorrecta(dadosoperacion);
           }else{
               //Volvemos a cada dado a su sitio
               for (const k in dadosoperacion) {
                   dadosoperacion[k].getComponent(Dado).retornarNumeros();
               }

           }
       }
    }
    crearHueco(indice: number){
        this.huecos[indice]= cc.instantiate(this.molde);
        this.huecos[indice].group='hueco';
        this.node.addChild( this.huecos[indice]);
        let xpos = (indice *  (this.huecos[indice].width+20));
        cc.log("Creamos el hueco "+indice+" a "+xpos);
        this.huecos[indice].setPosition(xpos+30,0);
    }
    start () {

    }

    public addDado(dadonodo: cc.Node){
        this.node.addChild(dadonodo);
        let xpos = (this.cuentadados *  (dadonodo.width+20));
        dadonodo.setPosition(xpos,0);
        this.cuentadados++;
        
        

    }

    public operacionCorrecta(dados: cc.Node[]){
        this.getComponent(cc.AudioSource).setCurrentTime(150);
        this.getComponent(cc.AudioSource).play();

        //De momento los quitamos
        for (const k in dados) {
            dados[k].removeFromParent();
            dados[k].getComponent(Dado).resetHueco();
        }

        //Actualizamos el marcador
        this.node.parent.parent.getComponent(JuegoLio).onOperacionCorrecta();
        

    }
    public limpiar(){
        let dados = this.node.children.filter((hijo: cc.Node) => hijo.group === 'dadocolocado' )
      
        for (const k in dados) {
            dados[k].getComponent(Dado).reset();
            dados[k].destroy();
        }
        //Cambiamos el grupo del resultado
     //   this.huecos[4].group = 'huecor';
    }
    private calcular(operadores: string[]): number{
        return OPERADOR[operadores[1]](+operadores[0],+operadores[2]);
    }

    // update (dt) {},
}
