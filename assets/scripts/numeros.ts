import Dado from "./dado";
import OperacionComponente from "./operacion";




// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Numeros extends cc.Component {

    @property(cc.Prefab)
    dado: cc.Prefab = null;

    @property(cc.Node)
    operacion: cc.Node = null;


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.log("Vamos a generar un dado");
        
    }

    start () {
        //this.generarDados(6);
        
    }

    public generarDados(numeros: string[],col: number){
        let fila=0;
        for(let n=0;n<numeros.length;n++){
            let c=n;
            if(n>0){
                fila=(Math.floor(n/col));
                c=n % col;
            }
            
            this.generaDado(c,fila,numeros[n]);
        }
    }

    private generaDado(col: number, fila: number, valor: string){
        cc.log("Vamos a generar un dado");
        let dadonode=cc.instantiate(this.dado)
        let dado: Dado = dadonode.getComponent(Dado);
        
        dado.setValor(valor);
        dado.initGroup('dado');
       dado.isdraggable = true;
        
        this.node.addChild(dadonode);
        dadonode.setPosition((col*(dadonode.width+20))+30,-(fila*(dadonode.height+20)));
        dado.storePosition();
        
    }

    public seleccionarDado(dadonodo: cc.Node){
        cc.log(dadonodo);
        dadonodo.removeFromParent();
        this.operacion.getComponent(OperacionComponente).addDado(dadonodo);
    }
    

    // update (dt) {},
}
