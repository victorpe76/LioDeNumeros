import Dado from "./dado";

// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ResultadosComponent extends cc.Component {

    @property(cc.Prefab)
    dado: cc.Prefab = null;

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
    },

    start () {

    },
    public generarDados(numeros: string[],col: number){
        cc.log(numeros);
        let fila=0;
        for(let n=0;n<numeros.length;n++){
            let c=n;
            
            
            this.generaDado(c,fila,numeros[n]);
        }
    }

    private generaDado(col: number, fila: number, valor: string){
        cc.log("Vamos a generar un dado");
        let dadonode=cc.instantiate(this.dado)
        let dado: Dado = dadonode.getComponent(Dado);
        
        dado.setValor(valor);
        dado.initGroup('dador');
        
       dado.isdraggable = true;

        
        this.node.addChild(dadonode);
       dadonode.color=new cc.Color(90,138,241,255);

        dadonode.setPosition(0,-(col*(dadonode.width+20)));
        dado.storePosition();
        
    }
    // update (dt) {},
}
