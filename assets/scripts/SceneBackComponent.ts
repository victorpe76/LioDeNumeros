const {ccclass, property} = cc._decorator;

@ccclass
export default class SceneBackComponent extends cc.Component {
    addBackSupport(): any {

        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function(keyCode, event) {
                if ((keyCode == cc.KEY.back) || (keyCode == cc.KEY.backspace) || (keyCode == cc.KEY.escape))
                    this.onTapBack();
            }.bind(this)
        }, this.node);
    }

    onLoad () {
      this.addBackSupport();  
    },
    onTapBack(){

    }
}