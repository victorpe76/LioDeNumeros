import { Puntuacion } from "./models/puntuacionbeans";
import { NIVELES } from "./models/levelbean";
import SceneBackComponent from "./SceneBackComponent";

// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends SceneBackComponent {

   private puntos: Puntuacion[];

    // LIFE-CYCLE CALLBACKS:

    @property(cc.Prefab)
    itemtiempo: cc.Prefab;

    @property(cc.Node)
    contenedoritems: cc.Node;

    onLoad () {
        super.onLoad();
        let puntuaciones=cc.sys.localStorage.
            getItem('puntuaciones');
        this.puntos= JSON.parse(puntuaciones);
        cc.log(puntuaciones);
        this.mostrarPuntuaciones();
    }

    start () {

    },

    volveraPrincipal(){
        cc.director.loadScene('principal');
    }
    onTapBack(){
        this.volveraPrincipal();
    }
    private mostrarPuntuaciones(){
        let strpuntos = '';
        for (const k in this.puntos) {
            let tiempo = cc.instantiate(this.itemtiempo);
            tiempo.getChildByName('texto').
                getComponent(cc.Label).string = NIVELES.find( n => n.id == this.puntos[k].nivel).nombre;
                tiempo.getChildByName('reloj').
                getComponent(cc.Label).string = this.puntos[k].tiempo;    
                tiempo.x=0;
                this.contenedoritems.addChild(tiempo);
        }
        
    }

    // update (dt) {},
}
