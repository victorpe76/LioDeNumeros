import { NivelJuego } from "./models/levelbean";

// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ItemJuego extends cc.Component {
    nivel: NivelJuego;



    @property
    texto: string = 'hello';


    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.getChildByName('texto').getComponent(cc.Label).string=this.nivel.nombre;

    },

    start () {

    },

    setOnClickListener(callback: cc.Component.EventHandler){
        this.node.getComponent(cc.Button).clickEvents.push(callback);
    }
    setNivel(nivel: NivelJuego){
        this.nivel=nivel;
        this.node.getChildByName('texto').getComponent(cc.Label).string=this.nivel.nombre;
        cc.log('Establecemos el nivel '+this.nivel.nombre);
    }
    isOK(t: boolean){
        if(t){
            this.node.getChildByName('numero').opacity=255;
        }else{
            this.node.getChildByName('numero').opacity=55;
        }
    }
    // update (dt) {},
}
