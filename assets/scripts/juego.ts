import { NivelJuego, NIVELES } from './models/levelbean';

import Numeros from './numeros';
import ResultadosComponent from './resultados';
import Marcador from './marcador';
import LevelManager from './LevelManager';
import OperacionComponente from './operacion';
import { Operacion } from './models/operacionbean';
import { Puntuacion } from './models/puntuacionbeans';
import Estrellas from './Estrellas';
import FindePartida from './finpartida';
// Learn TypeScript:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/typescript/index.html
// Learn Attribute:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/reference/attributes/index.html
// Learn life-cycle callbacks:
//  - [Chinese] http://www.cocos.com/docs/creator/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/editors_and_tools/creator-chapters/scripting/life-cycle-callbacks/index.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class JuegoLio extends cc.Component {
    tiempo: string;
    nextGame(): any {

        
         
        this.nivel = this.node.getComponent(LevelManager).playlevel;
        this.numeroOperaciones = this.node.getComponent(LevelManager).siguienteLevel();
        if(this.numeroOperaciones === 0){
            cc.log('FIN DE JUEGO-NIVEL');
            let tiempo=this.guardarTiempo(this.nivel.id);
       
            cc.director.loadScene('finpartida',(err:any,data:any) => {
                        let finpartida=cc.director.getScene();
                        
                        finpartida.getChildByName('finpartida')
                        .getComponent(FindePartida).fijarTiempo(tiempo);
            });
            return;

        }

        this.getComponent(cc.AudioSource).play();

       //De momento reciclamos
        this.generarOperaciones();
        //this.unscheduleAllCallbacks();
       this.estrellas.getComponent(Estrellas).levelHecho(this.numeroOperaciones-2);
       // this.marcadornivel.getComponent(cc.RichText).string="Nivel "+this.node.getComponent(LevelManager).currentlevel.toString()+" de "+
     if(this.inittime === undefined) this.initCrono();
        
    }
    initCrono(){
        this.inittime = new Date();
        
        this.schedule((elt) => {
            let now= new Date();
            let et =now.getTime()-this.inittime.getTime();

            cc.log(et);
            let dt: Date = new Date(et);

            let strformat: string=dt.toTimeString().split(' ')[0].substr(3,5);
           cc.log(strformat);
           // cc.log(strformat);
            this.node.getChildByName('tablero').getChildByName('crono')
                .getComponent(cc.Label).string=strformat;
        },1);
    }
    actualizarMarcador(): any {
        this.marcador.getComponent(Marcador).updateMarcador(this.operacionesok,this.operaciones.length)
        if(this.operacionesok===this.operaciones.length){
            this.nextGame();
        }
    }
    colmanager: cc.CollisionManager;

    @property
    numeroOperaciones: number = 4;

    @property(cc.Node)
    tableronumeros: cc.Node = null;

    @property(cc.Node)
    tableroresultados: cc.Node = null;

    @property(cc.Node)
    marcador: cc.Node = null;

    @property(cc.Node)
    estrellas: cc.Node = null;

    private operaciones: Operacion[]=[];
    private resultados: string[]=[];
    private numeros: string[]=[];
    private operacionesok: number = 0;
    private nivel: NivelJuego = null;

    private inittime: Date;

    

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        
       // this.nextGame();
        this.colmanager=cc.director.getCollisionManager();
        this.colmanager.enabled=true;
        
        
    }

    start () {

    },

    private generarOperaciones(){
        
        this.operaciones=[];
        this.resultados=[];
        this.numeros=[];
        this.operacionesok=0;

        //Limpiamos la operacion
        this.node.getChildByName('tablero').getChildByName('operacion')
        .getComponent(OperacionComponente).limpiar();


        for(let n=0;n<this.numeroOperaciones;n++){
            let operacion: Operacion;
            do{
                operacion=new Operacion(this.nivel.minimo,this.nivel.tope,this.nivel.tipo,this.nivel.numerosrepetidos);
           
            }while(!this.comprobarNumerosRepetidos(operacion));
            this.operaciones.push(operacion);
            this.resultados.push(operacion.getResultado());
            this.numeros=this.numeros.concat(operacion.getElementos());

        }

        //Barajamos los elementos
        this.shuffle(this.numeros);
        this.shuffle(this.resultados);

        //Invocamos el método del tablero
        this.tableronumeros.removeAllChildren(true)
        this.tableroresultados.removeAllChildren(true);
        this.tableronumeros.getComponent(Numeros).generarDados(this.numeros,3);
        this.tableroresultados.getComponent(ResultadosComponent).generarDados(this.resultados,4);
        //Establecemos el marcador
        this.actualizarMarcador();
    }

    private comprobarNumerosRepetidos(operacion: Operacion){

        let nrepetido=NIVELES[this.node.getComponent(LevelManager)
            .currentlevel].numerosrepetidos;
        
        if(nrepetido === undefined) {
            cc.log('No se han especificado numeros repetidos');
            return true;
        }

            let elementos=operacion.getElementos();
        
        for(const o in this.operaciones){
            for (const k in elementos) {
                if(this.operaciones[o].contarElemento(elementos[k])>nrepetido){
                    return false;
                }
            }
        }
        
        return true;
            
    }
    private shuffle(a: any[]) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    public getResultados(): string[]{
        return this.resultados;
    }
    public getNumeros(): string[]{
        return this.numeros;
    }

    public volverMenu(){
        cc.director.loadScene('principal');
    }
    // update (dt) {},

    public onOperacionCorrecta(){
        this.operacionesok++;
        this.actualizarMarcador();
    }

    private registraTiempo(){
        if(this.inittime!= null){
            
             
             let now= new Date();
                let et =now.getTime()-this.inittime.getTime();
    
                cc.log(et);
                let dt: Date = new Date(et);
    
                let t: string=dt.toTimeString().split(' ')[0].substr(3,5);
                
                
             
            }
    }
    private guardarTiempo(n: string): string{
        if(this.inittime!= null){
        let puntuaciones=cc.sys.localStorage.getItem('puntuaciones');
         
         let now= new Date();
            let et =now.getTime()-this.inittime.getTime();

            cc.log(et);
            let dt: Date = new Date(et);

            let t: string=dt.toTimeString().split(' ')[0].substr(3,5);
            let jptos: Puntuacion[];
            if(puntuaciones === null){
                jptos =[];
            }else{
                jptos =JSON.parse(puntuaciones);
            }
            
            
             jptos.push({nivel: n, tiempo: t});
             cc.sys.localStorage.setItem('puntuaciones',JSON.stringify(jptos));
             
             return t;
        }
    }
    backToPrincipal(){
        cc.director.loadScene('juegos');
    }
}
